﻿using UnityEngine;
using System.Collections.Generic;


namespace Enemies {

	public interface IEnemyListener {

		void OnEnemyDestroyed(Enemy enemy);
		void OnEnemyHightlightChange(Enemy enemy, bool hightlight);
	}

	public class Enemy : MonoBehaviour {

		[SerializeField] Dots dots = null;
		[SerializeField] SpriteRenderer spriteRenderer = null;
		[SerializeField] float HighlihtDelay = 1.0f;

		private Player target;
		private float velocity;
		private IEnemyListener listener;
		private float highlightTime;

		public void Initialize(IEnemyListener listener, Player target, float velocity, List<Color> colors) {

			HighlightedColor = false;
			this.listener = listener;
			this.target = target;
			this.velocity = velocity;
			dots.SetColors(colors);
		}

		private void Update() {

			if (HighlightedColor) {
				highlightTime -= Time.deltaTime;
				if (highlightTime <= 0.0f)
					Hightlight(false);
			}
		
			var nextPos = Vector2.MoveTowards(transform.position, target.transform.position, velocity * Time.deltaTime);
			if (nextPos == (Vector2)transform.position) {

				listener.OnEnemyDestroyed(this);
				Destroy(gameObject);
				target.Damage();
			}
			transform.position = nextPos;
		}

		private void OnDestroy() {
			
			listener = null;
		}

		public void Hightlight(bool value) {
			highlightTime = value ? HighlihtDelay : 0.0f;
			if (HighlightedColor != value) {
				HighlightedColor = value;
				listener.OnEnemyHightlightChange(this, value);
			}
		}

		public bool UseColor(Color color) {

			var result = dots.UseColor(color);
			if (result == UseDotResult.Destroyed) {
				listener.OnEnemyDestroyed(this);
				Destroy(gameObject);
			}
			return result != UseDotResult.Miss;
		}

		private bool HighlightedColor {
			get => spriteRenderer.color == Color.red;
			set => spriteRenderer.color = value ? Color.red : Color.gray;
		}
	}

}