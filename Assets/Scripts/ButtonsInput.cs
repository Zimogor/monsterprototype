﻿using UnityEngine;
using System;


public class ButtonsInput : MonoBehaviour {

	public Action<Color> OnColor;

	public void OnColorClick(int index) {

		OnColor?.Invoke(IndexToColor(index));
	}

	private Color IndexToColor(int index) {

		switch (index) {
			case 0:
				return Color.magenta;
			case 1:
				return Color.blue;
			case 2:
				return Color.yellow;
		}

		throw new Exception(index.ToString());
	}

	private void Update() {
		
		int index = -1;
		if (Input.GetKeyDown(KeyCode.A))
			index = 0;
		else if (Input.GetKeyDown(KeyCode.S))
			index = 1;
		else if (Input.GetKeyDown(KeyCode.D))
			index = 2;
		if (index == -1) return;

		OnColor?.Invoke(IndexToColor(index));
	}

	private void OnDestroy() {
		
		OnColor = null;
	}
}
