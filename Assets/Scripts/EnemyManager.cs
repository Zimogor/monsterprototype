﻿using UnityEngine;
using Enemies;
using System.Collections.Generic;


public class EnemyManager : MonoBehaviour, IEnemyListener {

	[Header("Parameters:")]
	[SerializeField] [Tooltip("Как быстро растёт сложность")]
	float complexityVelocity = 0.01f;
	[SerializeField] [Tooltip("Задержка между спаунами")]
	float startSpawnDelay = 2.0f;
	[SerializeField] [Tooltip("Как зависит задержка между спаунами от сложности")]
	float spawnDelayComplexityChange = 1.0f;
	[SerializeField] [Tooltip("Минимальное количество врагов")]
	float startMinAmount = 2.0f;
	[SerializeField] [Tooltip("Как быстро растёт минимальное количество врагов")]
	float minAmountComplexityChange = 10.0f;
	[SerializeField] [Tooltip("Начальное количество точек у врагов")]
	int startColorsAmount = 1;
	[SerializeField] [Tooltip("Как быстро растёт количество цветных точек у врагов")]
	float colorsComplexityChange = 7.0f;
	[SerializeField] [Tooltip("Максимальная скорость")]
	float startMaxVelocity = 1.0f;
	[SerializeField] [Tooltip("Как быстро растёт максимальная скорость")]
	float velocityComplexityChange = 3.0f;
	[SerializeField] [Tooltip("Разброс скоростей (1 - разброса не будет, 0 - разброс максимальный)")]
	float velocitySpread = 0.2f;
	[SerializeField] [Tooltip("Расстояние покраснения")]
	float frontDistance = 1.0f;

	[Header("Components:")]
	[SerializeField] ButtonsInput buttonsInput = null;
	[SerializeField] Player player = null;
	[SerializeField] Enemy enemyPrefab = null;
	[SerializeField] Rect spawnZone;

	private List<Enemy> enemies = new List<Enemy>();
	private List<Enemy> highlighted = new List<Enemy>();
	private List<Enemy> fronEnemieslistCache = new List<Enemy>();
	private List<Color> colorsCache = new List<Color>();
	private Color? colorClicked = null;
	private float nextSpawnDelay = 0.0f;
	private int minAmount = 2;
	private float complexity = 0.0f;
	private float spawnDelay = 2.0f;
	private int colorsAmount = 1;
	private float maxVelocity = 1.0f;

	private static readonly Color[] colorsSet = new Color[] { Color.magenta, Color.blue, Color.yellow };

	private void Awake() {
		
		buttonsInput.OnColor += (Color c) => colorClicked = c;
	}

	private void SpawnEnemy() {

		var posX = Random.Range(spawnZone.min.x, spawnZone.max.x);
		var posY = Random.Range(spawnZone.min.y, spawnZone.max.y);
		var enemy = Instantiate(enemyPrefab, new Vector3(posX, posY, 0.0f), Quaternion.identity, transform);
		colorsCache.Clear();
		var velocity = Random.Range(maxVelocity * Mathf.Min(1.0f, velocitySpread), maxVelocity);
		for (int i = 0; i < colorsAmount; i ++)
			colorsCache.Add(colorsSet[Random.Range(0, colorsSet.Length)]);
		enemy.Initialize(this, player, velocity, colorsCache);
		enemies.Add(enemy);
	}

	private void UpdateComplexityParameters() {

		complexity += Time.deltaTime * complexityVelocity;
		spawnDelay = startSpawnDelay - complexity * spawnDelayComplexityChange;
		minAmount = (int)(startMinAmount + complexity * minAmountComplexityChange);
		colorsAmount = Mathf.Min(6, (int)(startColorsAmount + complexity * colorsComplexityChange));
		maxVelocity = startMaxVelocity + complexity * velocityComplexityChange;
	}

	private void SpawnEnemies() {

		nextSpawnDelay -= Time.deltaTime;
		if (nextSpawnDelay <= 0.0f) {
			nextSpawnDelay += spawnDelay;
			SpawnEnemy();
			if (enemies.Count < minAmount) nextSpawnDelay = 0.0f;
		}
	}

	private void UpdateFront() {

		if (highlighted.Count == 1)
			highlighted[0].Hightlight(false);
		
		Enemy firstEnemy = null;
		foreach (var enemy in enemies) {
			if (firstEnemy == null) {
				firstEnemy = enemy;
				continue;
			}
			if (enemy.transform.position.y < firstEnemy.transform.position.y) {
				firstEnemy = enemy;
				continue;
			}
		}
		if (!firstEnemy) return;
		
		fronEnemieslistCache.Clear();
		foreach (var enemy in enemies) {
			if (Mathf.Abs(enemy.transform.position.y - firstEnemy.transform.position.y) <= frontDistance)
				fronEnemieslistCache.Add(enemy);
		}

		if (fronEnemieslistCache.Count > 1)
			foreach (var enemy in fronEnemieslistCache)
				enemy.Hightlight(true);

		if (colorClicked != null) {

			var gotTarget = false;
			if (highlighted.Count > 0) {

				for (int i = highlighted.Count - 1; i >= 0; i --) {
					if (highlighted[i].UseColor(colorClicked.Value))
						gotTarget = true;
				}
			} else if (firstEnemy.UseColor(colorClicked.Value))
				gotTarget = true;

			colorClicked = null;
			if (!gotTarget) player.Damage();
		}
	}

	private void LateUpdate() {

		UpdateComplexityParameters();
		SpawnEnemies();
		UpdateFront();
	}

	void IEnemyListener.OnEnemyDestroyed(Enemy enemy) {

		highlighted.Remove(enemy);
		enemies.Remove(enemy);
		if (enemies.Count < minAmount) nextSpawnDelay = 0.0f;
	}

	void IEnemyListener.OnEnemyHightlightChange(Enemy enemy, bool hightlight) {

		if (hightlight) highlighted.Add(enemy);
		else highlighted.Remove(enemy);
	}

	private void OnDrawGizmos() {
		
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(spawnZone.center, spawnZone.size);
	}
}
