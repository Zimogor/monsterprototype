﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


namespace Enemies {

	public enum UseDotResult { Miss, Target, Destroyed }

	public class Dots : MonoBehaviour {

		[SerializeField] Image[] dots = null;

		private int dotIndex = 0;

		public void SetColors(List<Color> colors) {

			for (int i = 0; i < colors.Count; i ++) {
				dots[i].gameObject.SetActive(true);
				dots[i].color = colors[i];
			}
		}

		public UseDotResult UseColor(Color color) {

			var result = UseDotResult.Miss;
			if (dots[dotIndex].color == color) {
				dots[dotIndex ++].gameObject.SetActive(false);
				result = UseDotResult.Target;
			}
			if (dotIndex >= dots.Length || !dots[dotIndex].gameObject.activeInHierarchy)
				result = UseDotResult.Destroyed;
			return result;
		}
	}

}