﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour {

	[SerializeField] int lives = 3;

	public void Damage() {

		if (--lives <= 0.0f)
			SceneManager.LoadScene(0);
	}
}
