﻿using UnityEngine;


public class CameraAligner : MonoBehaviour {

	[SerializeField] Transform target = null;

	private void Start() {
		
		var worldPos = GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.3f, 0.0f));
		transform.Translate((Vector2)target.position - (Vector2)worldPos);
	}
}
